package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;
import com.newdevelopcode.drawerexample.model.Categories;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.PlaceHolder>{

    int image[];
    String title[];
    onCvItemClick onCvItemClick;
    ArrayList<Categories> list;

    public CategoriesAdapter(ArrayList<Categories> list) {
        this.list = list;
    }
    public CategoriesAdapter(int[] image, String[] title) {
        this.image = image;
        this.title = title;
        this.onCvItemClick = onCvItemClick;
        this.list = list;
    }

    @NonNull
    @Override
    public CategoriesAdapter.PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item,null);
        PlaceHolder ph=new PlaceHolder(view);
        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoriesAdapter.PlaceHolder placeHolder, final int i) {
        placeHolder.title.setText(list.get(i).getTitle());
        placeHolder.imageView.setImageResource(list.get(i).getImage());
        placeHolder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCvItemClick.onItemClick(list.get(i).getTitle(),placeHolder.getAdapterPosition());
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView;
        CardView cardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
            imageView=itemView.findViewById(R.id.image);
            cardview=itemView.findViewById(R.id.card);
            
            
        }
    }

    /*@NonNull
    @Override*/
   /* public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(mContext).inflate(R.layout.list_item,viewGroup,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesAdapter.ViewHolder viewHolder, int i) {

        categories = cData.get(i);
        //Populate the textviews with data
        viewHolder.bindTo(categories);
        Glide.with(mContext).load(categories.getImage()).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return cData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
            image=itemView.findViewById(R.id.image);
        }
        void bindTo(Categories categories){
            //Populate the textviews with data
            title.setText(categories.getTitle());

        }
    }*/
   public interface onCvItemClick{
       public void onItemClick(String name,int position);
    }
    public void setOnCvItemClickListener(onCvItemClick onCvItemClick){
       this.onCvItemClick=onCvItemClick;
    }
}

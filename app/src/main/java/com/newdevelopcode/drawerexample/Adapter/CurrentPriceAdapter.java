
package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class CurrentPriceAdapter extends RecyclerView.Adapter<CurrentPriceAdapter.PlaceHolder> {
 String[] type;
 String[] riceCategories,pulsesCategories,onionCategories,garlicCategories,
         chiliCategories,palm_sugarCategories,jaggeryCategories,tamarindCategories,sugarCategories,potatoCategories,pazunkyawtCategories,freshwaterFishCategories,seawaterFishCategories;
 ArrayList<String> highPrice,onionPriceArray,garlcPriceArray,chiliPriceArray,palmSugarPriceArray,pulsesPriceArray,pazunPriceArray,jaggeryPriceArray,tamarindPriceArray,sugarPriceArray,potatoPriceArray,freshWaterFishPriceArray,seaWaterFishPriceArray;
    private RecyclerView.RecycledViewPool recycledViewPool=new RecyclerView.RecycledViewPool();




    public CurrentPriceAdapter(String[] type, ArrayList<String> highRicePriceArray) {
        this.type=type;
         highPrice=highRicePriceArray;
    }



    public CurrentPriceAdapter(String[] type, String[] riceCategories, String[] pulsesCategories, String[] onionCategories, String[] garlicCategories, String[] chiliCategories, String[] palm_sugarCategories, String[] jaggeryCategories, String[] tamarindCategories, String[] sugarCategories, String[] potatoCategories, String[] pazunkyawtCategories, String[] freshwaterFishCategories, String[] seawaterFishCategories, ArrayList<String> highRicePriceArray, ArrayList<String> pulsesPriceArray, ArrayList<String> onionPriceArray, ArrayList<String> garlicPriceArray, ArrayList<String> chiliPriceArray,ArrayList<String> palmSugarPriceArray, ArrayList<String> jaggeryPriceArray, ArrayList<String> tamarindPriceArray, ArrayList<String> sugarPriceArray, ArrayList<String> potatoPriceArray, ArrayList<String> pazunPriceArray, ArrayList<String> freshWaterFishPriceArray, ArrayList<String> seaWaterFishPriceArray) {
        this.type=type;
        this.riceCategories=riceCategories;
        this.pulsesCategories=pulsesCategories;
        this.onionCategories=onionCategories;
        this.garlicCategories=garlicCategories;
        this.chiliCategories=chiliCategories;
        this.palm_sugarCategories=palm_sugarCategories;
        this.jaggeryCategories=jaggeryCategories;
        this.tamarindCategories=tamarindCategories;
        this.sugarCategories=sugarCategories;
        this.potatoCategories=potatoCategories;
        this.pazunkyawtCategories=pazunkyawtCategories;
        this.freshwaterFishCategories=freshwaterFishCategories;
        this.seawaterFishCategories=seawaterFishCategories;

        this.pulsesPriceArray=pulsesPriceArray;
        this.onionPriceArray=onionPriceArray;
        this.chiliPriceArray=chiliPriceArray;
        this.palmSugarPriceArray=palmSugarPriceArray;
        this.jaggeryPriceArray=jaggeryPriceArray;
        this.seaWaterFishPriceArray=seaWaterFishPriceArray;
        this.freshWaterFishPriceArray=freshWaterFishPriceArray;
        this.sugarPriceArray=sugarPriceArray;
        this.tamarindPriceArray=tamarindPriceArray;
        this.potatoPriceArray=potatoPriceArray;
        this.pazunPriceArray=pazunPriceArray;
        this.garlcPriceArray=garlicPriceArray;

        //lowPrice=lowRicePriceArray;
        highPrice=highRicePriceArray;
    }


    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_current_price_layout,null);
        PlaceHolder ph=new PlaceHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
        placeHolder.tvCurrentType.setText(type[i]);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(placeHolder.typeRecycler.getContext(), LinearLayoutManager.VERTICAL,false);
        //linearLayoutManager.setInitialPrefetchItemCount(0);
        if(i==0) {
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(riceCategories,highPrice);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==1){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(pulsesCategories,pulsesPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==2){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(onionCategories,onionPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==3){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(garlicCategories,garlcPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==4){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(chiliCategories,chiliPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==5){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(palm_sugarCategories,palmSugarPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==6){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(jaggeryCategories,jaggeryPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==7){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(tamarindCategories,tamarindPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==8){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(sugarCategories,sugarPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==9){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(potatoCategories,potatoPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==10){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(pazunkyawtCategories,pazunPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==11){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(freshwaterFishCategories,freshWaterFishPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
        else if(i==12){
            CurrentTypeAdapter cbAdapter = new CurrentTypeAdapter(seawaterFishCategories,seaWaterFishPriceArray);
            placeHolder.typeRecycler.setLayoutManager(linearLayoutManager);
            placeHolder.typeRecycler.setAdapter(cbAdapter);
            placeHolder.typeRecycler.setRecycledViewPool(recycledViewPool);
        }
    }

    @Override
    public int getItemCount() {
        return type.length;
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvCurrentType;
        RecyclerView typeRecycler;
        CardView ricecardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvCurrentType=itemView.findViewById(R.id.tvv_current_type);
            typeRecycler=itemView.findViewById(R.id.type_recycler);



        }
    }
}








package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class CurrentRiceTypeAdapter extends RecyclerView.Adapter<CurrentRiceTypeAdapter.PlaceHolder> {

    ArrayList<String>  highPrice;
    String[] riceCategories,pulsesCategories,onionCategories,garlicCategories,
            chiliCategories,palm_sugarCategories,jaggeryCategories,tamarindCategories,sugarCategories,
            potatoCategories,pazunkyawtCategories,freshwaterFishCategories,seawaterFishCategories;



    public CurrentRiceTypeAdapter(String[] riceCategories, ArrayList<String> highPrice) {
        this.highPrice=highPrice;
        this.riceCategories=riceCategories;
    }


    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.child_type_current_layout,null);
        PlaceHolder ph=new PlaceHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
       // placeHolder.tvDate.setText(date.get(i));
        placeHolder.tvType.setText(riceCategories[i]);
        placeHolder.tvHighPrice.setText(highPrice.get(i));
    }

    @Override
    public int getItemCount() {
        return highPrice.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvType,tvHighPrice;
        CardView ricecardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvType=itemView.findViewById(R.id.type);
             tvHighPrice=itemView.findViewById(R.id.price);

        }
    }
}

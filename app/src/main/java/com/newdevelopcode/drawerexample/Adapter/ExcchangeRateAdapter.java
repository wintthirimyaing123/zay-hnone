package com.newdevelopcode.drawerexample.Adapter;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

public class ExcchangeRateAdapter extends RecyclerView.Adapter<ExcchangeRateAdapter.Placeholder> {
    String[] countryRef;
    String[] rate;
    int flag[];

    public ExcchangeRateAdapter(String[] countryRef, String[] rate, int[] flag) {
        this.countryRef=countryRef;
        this.rate=rate;
        this.flag=flag;

    }

    @Override
    public Placeholder onCreateViewHolder( ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_exchange_rate_layout,null);
        Placeholder ph=new Placeholder(view);
        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull Placeholder placeholder, int i) {
        placeholder.tvExchangeRateRef.setText(countryRef[i]);
        placeholder.tvExchangeRate.setText(rate[i]);
        placeholder.imgflag.setImageResource(flag[i]);


    }


    @Override
    public int getItemCount() {
        return countryRef.length;
    }

    public class Placeholder extends RecyclerView.ViewHolder {
        TextView tvExchangeRateRef,tvExchangeRate;
        ImageView imgflag;
        public Placeholder( View itemView) {
            super(itemView);
           tvExchangeRateRef=itemView.findViewById(R.id.tv_exchangerate_ref);
           tvExchangeRate=itemView.findViewById(R.id.tv_exchangerate_rate);
           imgflag=itemView.findViewById(R.id.imgFlag);

        }
    }
}

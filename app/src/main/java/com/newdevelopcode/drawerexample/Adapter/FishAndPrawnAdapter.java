package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class FishAndPrawnAdapter extends RecyclerView.Adapter<FishAndPrawnAdapter.PlaceHolder>{

    ArrayList<String> dateArray,chickenArray,porkArray,beefArray,muttonArray,ngakhuArray,ngagyiArray,ngathalaukArray,ngagyinArray,ngamyitchinArray,pazundokeArray,pazunkyawtArray;


    public FishAndPrawnAdapter(ArrayList<String> dateArray, ArrayList<String> chickenArray, ArrayList<String> porkArray, ArrayList<String> beefArray, ArrayList<String> muttonArray, ArrayList<String> ngakhuArray, ArrayList<String> ngagyiArray, ArrayList<String> ngathalaukArray, ArrayList<String> ngagyinArray, ArrayList<String> ngamyitchinArray, ArrayList<String> pazundokeArray, ArrayList<String> pazunkyawtArray) {
        this.dateArray=dateArray;
        this.chickenArray=chickenArray;
        this.porkArray=porkArray;
        this.beefArray=beefArray;
        this.muttonArray=muttonArray;
        this.ngagyiArray=ngagyiArray;
        this.ngakhuArray=ngakhuArray;
        this.ngagyinArray=ngagyinArray;
        this.ngathalaukArray=ngathalaukArray;
        this.ngamyitchinArray=ngamyitchinArray;
        this.pazundokeArray=pazundokeArray;
        this.pazunkyawtArray=pazunkyawtArray;
    }

    @NonNull
    @Override
    public FishAndPrawnAdapter.PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_fish_and_prawn,null);
       PlaceHolder ph=new PlaceHolder(view);
        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull FishAndPrawnAdapter.PlaceHolder placeHolder, int i) {
        placeHolder.date.setText(dateArray.get(i));
        placeHolder.chicken.setText(chickenArray.get(i));
        placeHolder.pork.setText(porkArray.get(i));
        placeHolder.beef.setText(beefArray.get(i));
        placeHolder.mutton.setText(muttonArray.get(i));
        placeHolder.ngathalauk.setText(ngathalaukArray.get(i));
        placeHolder.ngagyi.setText(ngagyiArray.get(i));
        placeHolder.ngagyin.setText(ngagyinArray.get(i));
        placeHolder.ngamyitchin.setText(ngamyitchinArray.get(i));
        placeHolder.ngakhu.setText(ngakhuArray.get(i));
        placeHolder.pazudoke.setText(pazundokeArray.get(i));
        placeHolder.pazukyawt.setText(pazunkyawtArray.get(i));
    }

    @Override
    public int getItemCount() {
        return dateArray.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {

        TextView date,chicken,pork,beef,mutton,ngagyi,ngakhu,ngagyin,ngamyitchin,ngathalauk,pazudoke,pazukyawt;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.tv_date);
            chicken=itemView.findViewById(R.id.chicken);
            pork=itemView.findViewById(R.id.pork);
            beef=itemView.findViewById(R.id.beef);
            mutton=itemView.findViewById(R.id.mutton);
            ngagyi=itemView.findViewById(R.id.ngagyi);
            ngakhu=itemView.findViewById(R.id.ngakhu);
            ngagyin=itemView.findViewById(R.id.ngagyin);
            ngathalauk=itemView.findViewById(R.id.ngathalauk);
            ngamyitchin=itemView.findViewById(R.id.ngamyitchin);
            pazudoke=itemView.findViewById(R.id.pazundoke);
            pazukyawt=itemView.findViewById(R.id.pazunkyawt);
        }
    }
}

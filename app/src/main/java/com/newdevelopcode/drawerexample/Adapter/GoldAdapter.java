package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class GoldAdapter extends RecyclerView.Adapter<GoldAdapter.PlaceHolder> {

    ArrayList<String> unit,price,previousPrice;

    public GoldAdapter(ArrayList<String> unitArray, ArrayList<String> priceArray, ArrayList<String> previousPriceArray) {
        this.unit=unitArray;
        this.previousPrice=previousPriceArray;
        this.price=priceArray;
    }

    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_gold_fragment_layout,null);
        PlaceHolder ph=new PlaceHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
        placeHolder.tvUnit.setText(unit.get(i));
        placeHolder.tvPrice.setText(price.get(i));
        placeHolder.tvPreviousPrice.setText(previousPrice.get(i));
    }

    @Override
    public int getItemCount() {
        return unit.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvUnit,tvPrice,tvPreviousPrice;
        CardView ricecardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvUnit=itemView.findViewById(R.id.tv_unit);
            tvPrice=itemView.findViewById(R.id.tv_price);
            tvPreviousPrice=itemView.findViewById(R.id.tv_previousPrice);

        }
    }
}

package com.newdevelopcode.drawerexample.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class ImageSwipeAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;

    public ImageSwipeAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragments = fragmentList;
    }

    @Override
    public Fragment getItem(int i) {
        /*Fragment fragment=new ImageSwipe();
        Bundle args=new Bundle();
        args.putInt("count",i+1);
        fragment.setArguments(args);*/
        return this.fragments.get(i);
    }
    @Override
    public int getCount() {
        return this.fragments.size();
    }
}


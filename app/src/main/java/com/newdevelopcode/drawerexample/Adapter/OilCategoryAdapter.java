package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class OilCategoryAdapter extends RecyclerView.Adapter<OilCategoryAdapter.PlaceHolder> {

    ArrayList<String> date,groundnutSpecial,groundnutMedium,sesamum,palm;

    public OilCategoryAdapter(ArrayList<String> date, ArrayList<String> groundnutSpecial, ArrayList<String> groundnutMedium, ArrayList<String> sesamum, ArrayList<String> palm) {
        this.date = date;
        this.groundnutSpecial = groundnutSpecial;
        this.groundnutMedium = groundnutMedium;
        this.sesamum = sesamum;
        this.palm = palm;
    }

    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_oil_layout,
                null);
        PlaceHolder ph=new PlaceHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
            placeHolder.tvDate.setText(date.get(i));
            placeHolder.tvgroundnutspecial.setText(groundnutSpecial.get(i));
            placeHolder.tvgroundnutmedium.setText(groundnutMedium.get(i));
            placeHolder.tvsesamum.setText(sesamum.get(i));
            placeHolder.tvpalm.setText(palm.get(i));

    }

    @Override
    public int getItemCount() {
        return date.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvDate,tvgroundnutspecial,tvgroundnutmedium,tvsesamum,tvpalm;
        CardView ricecardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvDate=itemView.findViewById(R.id.tv_date);
            tvgroundnutspecial=itemView.findViewById(R.id.groundnut_oil_special);
            tvgroundnutmedium=itemView.findViewById(R.id.groundnut_oil_medium);
            tvsesamum=itemView.findViewById(R.id.sesamum_oil);
            tvpalm=itemView.findViewById(R.id.palm_oil);
            ricecardview=itemView.findViewById(R.id.oilcard);

        }
    }
}

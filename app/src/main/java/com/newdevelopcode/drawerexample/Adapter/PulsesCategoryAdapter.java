package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class PulsesCategoryAdapter extends RecyclerView.Adapter<PulsesCategoryAdapter.PlaceHolder> {
    ArrayList<String> date,gramPulses,penilayPulses,pegyiPulses,satawpePulses;

    public PulsesCategoryAdapter(ArrayList<String> date, ArrayList<String> gramPulses, ArrayList<String> penilyPulses, ArrayList<String> pegyiPulses, ArrayList<String> satawpePulses) {
        this.date = date;
        this.gramPulses = gramPulses;
        this.penilayPulses = penilyPulses;
        this.pegyiPulses = pegyiPulses;
        this.satawpePulses = satawpePulses;
    }

    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_pulses_layout,null);
        PlaceHolder ph=new PlaceHolder(v);
        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
        placeHolder.tvDate.setText(date.get(i));
        placeHolder.tvGram.setText(gramPulses.get(i));
        placeHolder.tvPenilay.setText(penilayPulses.get(i));
        placeHolder.tvPegyi.setText(pegyiPulses.get(i));
        placeHolder.tvSatawpe.setText(satawpePulses.get(i));

    }

    @Override
    public int getItemCount() {
        return date.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvDate,tvGram,tvPenilay,tvPegyi,tvSatawpe;
        CardView pulsescardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tv_date);
            tvGram = itemView.findViewById(R.id.rate_gram);
            tvPenilay = itemView.findViewById(R.id.rate_penilay);
            tvPegyi = itemView.findViewById(R.id.rate_pegyi);
            tvSatawpe = itemView.findViewById(R.id.rate_satawpe);
            pulsescardview = itemView.findViewById(R.id.pulsesCard);
        }
    }
}

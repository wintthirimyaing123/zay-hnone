package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class RiceCategoryAdapter extends RecyclerView.Adapter<RiceCategoryAdapter.PlaceHolder> {

    ArrayList<String> date,emataRice,pawsanRice;

    public RiceCategoryAdapter(ArrayList<String> dateArray, ArrayList<String> emataArray, ArrayList<String> pawsanArray) {
           this.date=dateArray;
           this.emataRice=emataArray;
           this.pawsanRice=pawsanArray;
    }


    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_rice_layout,null);
        PlaceHolder ph=new PlaceHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceHolder placeHolder, int i) {
            placeHolder.tvDate.setText(date.get(i));
            placeHolder.tvEmata.setText(emataRice.get(i));
            placeHolder.tvPawsan.setText(pawsanRice.get(i));
    }

    @Override
    public int getItemCount() {
        return date.size();
    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        TextView tvDate,tvEmata,tvPawsan;
        CardView ricecardview;
        public PlaceHolder(@NonNull View itemView) {
            super(itemView);

            tvDate=itemView.findViewById(R.id.tv_date);
            tvEmata=itemView.findViewById(R.id.rate_emata);
            tvPawsan=itemView.findViewById(R.id.rate_pawsan);
            ricecardview=itemView.findViewById(R.id.ricecard);

        }
    }
}

package com.newdevelopcode.drawerexample.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;

public class SpicesCategoryAdapter extends RecyclerView.Adapter<SpicesCategoryAdapter.Placeholder> {

    ArrayList<String> date,chillies,garlic,onion,potato;

    public SpicesCategoryAdapter(ArrayList<String> date, ArrayList<String> gram, ArrayList<String> garlic, ArrayList<String> onion, ArrayList<String> potato) {
        this.date = date;
        this.chillies = gram;
        this.garlic = garlic;
        this.onion = onion;
        this.potato = potato;
    }

    @NonNull
    @Override
    public Placeholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_spices_layout,null);
        Placeholder placeholder=new Placeholder(v);
        return placeholder;
    }

    @Override
    public void onBindViewHolder(@NonNull Placeholder placeholder, int i) {

        placeholder.tvDate.setText(date.get(i));
        placeholder.tvChillies.setText(chillies.get(i));
        placeholder.tvGarlic.setText(garlic.get(i));
        placeholder.tvOnion.setText(onion.get(i));
        placeholder.tvPotato.setText(potato.get(i));
    }

    @Override
    public int getItemCount() {
        return date.size();
    }

    public class Placeholder extends RecyclerView.ViewHolder {

        TextView tvDate,tvChillies,tvGarlic,tvOnion,tvPotato;
        CardView spicesCardView;
        public Placeholder(@NonNull View itemView) {
            super(itemView);

            tvDate=itemView.findViewById(R.id.tv_date);
            tvChillies=itemView.findViewById(R.id.rate_chillies);
            tvGarlic=itemView.findViewById(R.id.rate_garlic);
            tvOnion=itemView.findViewById(R.id.rate_onion);
            tvPotato=itemView.findViewById(R.id.rate_potato);
            spicesCardView=itemView.findViewById(R.id.spicescard);
        }
    }
}

package com.newdevelopcode.drawerexample.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.R;

public class AboutFragment extends Fragment {
    View view;
    TextView textView ;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_about,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("About");

        textView=view.findViewById(R.id.text);
        textView.setText("ဤApp၏ေစ်းနႈန္းမ်ားသည္ ျမန္မာနိုင္ငံအစိုးရမွတရားဝင္ထုတ္ျပန္ထားေသာwebsiteမ်ား၏ေစ်းနႈန္းမ်ားကိုရယူ၍ေရးသားထားျခင္းျဖစ္ပါသည္္။ဤAppတြင္ အစိုးရရံုးပိတ္ရက္မ်ား၌ေစ်းနႈန္းမ်ားကို ထုတ္ျပန္ထားျခင္းမရွိပါ။ဤAppတြင္္ ပါဝင္ေသာေစ်းနႈန္းမ်ားကို ရယူခြင့္ေပးေသာ websiteမ်ားအား အထူးေက်းဇူးတင္ပါသည္္။ေစ်းျခင္းappနွင့္ပတ္သတ္ျပီး လိုအပ္ခ်က္မ်ား|ေဝဖန္အႀကံျပဳခ်က္မ်ား ေပးပို႔မည္ဆိုပါက အေလးထားေဆာင္ရြက္ေပးသြားပါမည္။");
        //advise=view.findViewById(R.id.tv_advise);
       /* share=view.findViewById(R.id.tv_share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent=new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText="Check it out.Your message goes here";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,shareBodyText);
                startActivity(Intent.createChooser(sharingIntent,"Sharing Option"));
            }
        });*/
        return view;



    }
}

package com.newdevelopcode.drawerexample.Fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.ExcchangeRateAdapter;
import com.newdevelopcode.drawerexample.R;
import com.newdevelopcode.drawerexample.model.Currencies;
import com.newdevelopcode.drawerexample.services.AllCurrencies;
import com.newdevelopcode.drawerexample.services.ApiUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExchangeRateFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    View view;
   public String[] rate=new String[38];
   String AUD,BDT,BND,BRL,CAD,CHF,CNY,CZK,DKK,EGP,EUR,GBP,HKD,IDR,ILS,INR,JPY,KES,KHR,KRW,KWD,LAK,LKR,MYR,NOK,NPR,NZD,PHP,PKR,RSD,RUB,SAR,SEK,SGD,THB,USD,VND,ZAR;
    String[] countryRef= new String[]{
            "Australian Dollar",
            "Bangladesh Taka","Brunei Dollar","Brazilian Real",
            "Canadian Dollar","Swiss Franc","Chinese Yann","Czech koruna",
            "Danish Krone",
            "Egyption Pound","Euro",
            "Pound Sterling",
            "Hong Kong Dollar",
            "Indonesian Rupiah","Israeli Shekel","Indian Ruphtee",
            "Japanese Yan",
            "Kenya Shilling","Cambodian Riel","Korean Won","Kuwaiti Dinar",
            "Lao Kip","Sri Lankan Rupee",
            "Malaysian Ringgit",
            "Norwegian Kroner","Nepalese Rupee","New Zeeland Dollar",
            "Philippines Peso","Pakistani Rupee",
            "Serbian Dinar","Russian Rouble",
            "Saudi Arabian Riyal","Swedish Krona", "Singapore Dollar ",
            "Thai baht",
            "United State Dollar",
            "Vietnamese Dong",
           "South Africa Rand",



    };
    int flag[]={R.drawable.australian_dollar_aud,
            R.drawable.bangladeshtaka_bdt,R.drawable.brunei_dollar_bnd,R.drawable.brazilianreal_brl,
            R.drawable.cambodian_riel_khr,R.drawable.canadian_dollar_cad,R.drawable.china_cny,R.drawable.czech_koruna_czk,
            R.drawable.danishkrone_dkk,
            R.drawable.egyptianpound_egp,R.drawable.europeanunion,
            R.drawable.hongkongdollar_hkd,
            R.drawable.indian_rupee_inr,R.drawable.indonesianrupiah_idr,R.drawable.lsraelisheke_ils,
            R.drawable.japanese_yen_jpy,
            R.drawable.kenyashilling_kes,R.drawable.korean_won_krw,R.drawable.kuwaitidinar_kwd,
            R.drawable.laos,
            R.drawable.malaysian_ringgit_myr,
            R.drawable.nepaleserupee_nrp,R.drawable.new_zealand_dolla_nzd,R.drawable.norwegiankroner_nok,
            R.drawable.prp,R.drawable.phillippines_php,R.drawable.pound_starling_gbp,
            R.drawable.russianrouble_rub,
            R.drawable.saudiarabianriyal_sar,R.drawable.serbiandinar_rsd,R.drawable.singapore_sgd,R.drawable.southafricarandzra,R.drawable.srilankanrupee_lkp,R.drawable.swedishkrona,R.drawable.swiss_franc_chf,
            R.drawable.thaibaht_thb,
            R.drawable.usd,
            R.drawable.vietnamesedong_vnd
    };
    RecyclerView exchangeRecycler;
    SwipeRefreshLayout swipeRefreshLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_exchangerate,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("ေငြလဲလွယ္ႏႈန္း");
        exchangeRecycler=view.findViewById(R.id.exchangeraterecycler);
        swipeRefreshLayout=view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        AllCurrencies allCurrencie= ApiUtil.getAllCurrencies();
        allCurrencie.getCurrencies().enqueue(new Callback<Currencies>() {
            @Override
            public void onResponse(Call<Currencies> call, Response<Currencies> response) {
                if(response.isSuccessful()){
                     USD=response.body().getRates().getUSD();
                     AUD=response.body().getRates().getAUD();
                     BDT=response.body().getRates().getBDT();
                     BND=response.body().getRates().getBND();
                     BRL=response.body().getRates().getBRL();
                     CAD=response.body().getRates().getCAD();
                     CHF=response.body().getRates().getCHF();
                     CNY=response.body().getRates().getCNY();
                     CZK=response.body().getRates().getCZK();
                     DKK=response.body().getRates().getDKK();

                     EGP=response.body().getRates().getEGP();
                     EUR=response.body().getRates().getEUR();
                     GBP=response.body().getRates().getGBP();
                     HKD=response.body().getRates().getHKD();
                     IDR=response.body().getRates().getIDR();
                     ILS=response.body().getRates().getILS();
                     INR=response.body().getRates().getINR();
                     JPY=response.body().getRates().getJPY();
                     KES=response.body().getRates().getKES();
                     KHR=response.body().getRates().getKHR();

                     KRW=response.body().getRates().getKRW();
                     KWD=response.body().getRates().getKWD();
                     LAK=response.body().getRates().getLAK();
                     LKR=response.body().getRates().getLKR();
                     MYR=response.body().getRates().getMYR();
                     NOK=response.body().getRates().getNOK();
                     NPR=response.body().getRates().getNPR();
                     NZD=response.body().getRates().getNZD();
                     PHP=response.body().getRates().getPHP();
                     PKR=response.body().getRates().getPKR();

                     RSD=response.body().getRates().getRSD();
                     RUB=response.body().getRates().getRUB();
                     SAR=response.body().getRates().getSAR();
                     SEK=response.body().getRates().getSEK();
                     SGD=response.body().getRates().getSGD();
                     THB=response.body().getRates().getTHB();
                     VND=response.body().getRates().getVND();
                     ZAR=response.body().getRates().getZAR();

                }
                rate= new String[]{AUD,BDT,BND,BRL,CAD,CHF,CNY,CZK,DKK,EGP,EUR,GBP,HKD,IDR,ILS,INR,JPY,KES,KHR,KRW,KWD,LAK,LKR,MYR,NOK,NPR,NZD,PHP,PKR,RSD,RUB,SAR,SEK,SGD,THB,USD,VND,ZAR};
                ExcchangeRateAdapter adapter=new ExcchangeRateAdapter(countryRef,rate,flag);
                exchangeRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                exchangeRecycler.setHasFixedSize(true);
                exchangeRecycler.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<Currencies>   call, Throwable t) {
                isOnline();
            }
        });

        return view;
    }
    public boolean isOnline(){
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}

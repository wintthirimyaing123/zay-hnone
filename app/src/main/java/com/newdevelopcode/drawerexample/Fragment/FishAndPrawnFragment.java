package com.newdevelopcode.drawerexample.Fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.FishAndPrawnAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class FishAndPrawnFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView fishRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    ArrayList<String> dateArray;
    ArrayList<String> chickenArray,porkArray,beefArray,muttonArray,ngakhuArray,ngagyiArray,
            ngathalaukArray,ngagyinArray,ngamyitchinArray,pazundokeArray,pazunkyawtArray;

    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    int currentImageIndex=0;
    int[] imgResources={R.drawable.fishnew1,R.drawable.fishnew3,
            R.drawable.fishnew4,R.drawable.fishnew5};

     @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate
                (R.layout.fishandprawn_layout,container,false);
         ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("အသားငါးေဈးႏႈန္းမ်ား");

        fishRecyclerView=view.findViewById(R.id.fishRecycler);
        swipeRefreshLayout=view.findViewById(R.id.swipe);
         slidingimage=view.findViewById(R.id.imageview);
         final Handler myHandler=new Handler();
         final Runnable myUpdateResults=new Runnable() {
             @Override
             public void run() {
                 AnimationSlideShow();
             }
         };
         int delay=1000;//delay for 1 second
         int period=1500;//repeat every 4 second
         Timer timer=new Timer();
         timer.scheduleAtFixedRate(new TimerTask() {
             @Override
             public void run() {
                 myHandler.post(myUpdateResults);
             }
         },delay,period);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);


        checkConnection();

        return view;
    }
    private void AnimationSlideShow() {
        slidingimage.setImageResource(imgResources[currentImageIndex%imgResources.length]);
        currentImageIndex++;
    }
    public void checkConnection(){
        if(isOnline()){
            new ParseData().execute();

//            handler.postDelayed(run,1000);
            swipeRefreshLayout.setRefreshing(false);
        }
    }
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            new ParseData().execute();        }
    };

    public class ParseData extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {

            if(!isOnline()){
                handler.removeCallbacks(runnable);
            }
            try {
                swipeRefreshLayout.setRefreshing(false);

                Document document= Jsoup.connect("https://www.csostat.gov.mm/market.asp").get();
                Elements table=document.select("table[class=btext1]");
                Elements row=table.select("tr");
                Element firstRow=row.get(7);
                Element chicken=row.get(34);
                Element pork=row.get(35);
                Element beef=row.get(36);
                Element mutton=row.get(37);
                Element ngakhu=row.get(38);
                Element ngagyi=row.get(39);
                Element ngathalauk=row.get(40);
                Element ngagyin=row.get(41);
                Element ngamyitchin=row.get(42);
                Element pazundoke=row.get(43);
                Element pazunkyawt=row.get(44);

                dateArray=new ArrayList<>();

                chickenArray=new ArrayList<>();
                porkArray=new ArrayList<>();
                beefArray=new ArrayList<>();
                muttonArray=new ArrayList<>();
                ngakhuArray=new ArrayList<>();
                ngagyiArray=new ArrayList<>();
                ngathalaukArray=new ArrayList<>();
                ngagyinArray=new ArrayList<>();
                ngamyitchinArray=new ArrayList<>();
                pazundokeArray=new ArrayList<>();
                pazunkyawtArray=new ArrayList<>();

                Elements dateColumn=firstRow.select("td");
                for(int i=2;i<dateColumn.size();i++){
                    String date=dateColumn.get(i).text();
                    dateArray.add(date);

                }
                Elements chickenColumn=chicken.select("td");
                for(int i=2;i<chickenColumn.size();i++){
                    String date=chickenColumn.get(i).text();
                    Log.d("test","value is"+date);

                    chickenArray.add(date);

                }

                Elements porkColumn=pork.select("td");
                for(int i=2;i<porkColumn.size();i++){
                    String date=porkColumn.get(i).text();
                    porkArray.add(date);

                }

                Elements beefColumn=beef.select("td");
                for(int i=2;i<beefColumn.size();i++){
                    String date=beefColumn.get(i).text();
                    beefArray.add(date);

                }

                Elements muttonColumn=mutton.select("td");
                for(int i=2;i<muttonColumn.size();i++){
                    String date=muttonColumn.get(i).text();
                    muttonArray.add(date);

                }
                Elements ngakhuColumn=ngakhu.select("td");
                for(int i=2;i<ngakhuColumn.size();i++){
                    String date=ngakhuColumn.get(i).text();
                    ngakhuArray.add(date);

                }

                Elements ngagyiColumn=ngagyi.select("td");
                for(int i=2;i<ngagyiColumn.size();i++){
                    String date=ngagyiColumn.get(i).text();
                    ngagyiArray.add(date);

                }
                Elements ngathalaukColumn=ngathalauk.select("td");
                for(int i=2;i<ngathalaukColumn.size();i++){
                    String date=ngathalaukColumn.get(i).text();
                    ngathalaukArray.add(date);

                }

                Elements ngagyinColumn=ngagyin.select("td");
                for(int i=2;i<ngagyinColumn.size();i++){
                    String date=ngagyinColumn.get(i).text();
                    ngagyinArray.add(date);

                }
                Elements ngamyitchinColumn=ngamyitchin.select("td");
                for(int i=2;i<ngamyitchinColumn.size();i++){
                    String date=ngamyitchinColumn.get(i).text();
                    ngamyitchinArray.add(date);

                }

                Elements pazundokeColumn=pazundoke.select("td");
                for(int i=2;i<pazundokeColumn.size();i++){
                    String date=pazundokeColumn.get(i).text();
                    pazundokeArray.add(date);

                }
                Elements pazunkyawtColumn=pazunkyawt.select("td");
                for(int i=2;i<pazunkyawtColumn.size();i++){
                    String date=pazunkyawtColumn.get(i).text();
                    pazunkyawtArray.add(date);

                }




            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            FishAndPrawnAdapter fishAndPrawnAdapter=new FishAndPrawnAdapter(dateArray,chickenArray,porkArray,beefArray,muttonArray,ngakhuArray,ngagyiArray,
                    ngathalaukArray,ngagyinArray,ngamyitchinArray,pazundokeArray,pazunkyawtArray);
            fishRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            fishRecyclerView.setHasFixedSize(true);
            fishRecyclerView.setAdapter(fishAndPrawnAdapter);
        }
    }

    public boolean isOnline(){
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
    }

    @Override
    public void onRefresh() {

        swipeRefreshLayout.setRefreshing(false);
    }
}

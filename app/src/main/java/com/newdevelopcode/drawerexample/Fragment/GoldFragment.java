package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.icu.util.RangeValueIterator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.Adapter.GoldAdapter;
import com.newdevelopcode.drawerexample.Adapter.RiceCategoryAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class GoldFragment extends Fragment {
    View view;
    Handler handler;
    String percentage,priceTitle,previousPriceTitle;
    String unit,lastDate;
    TextView tvPrice,tvPrevious,tvPercentage,tvUnit,lastUpdate;
    RecyclerView goldRecycler;
    ArrayList<String> arrayUnit,arrayPrice,arrayPreviousPrice;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_gold,container,false);

        goldRecycler=view.findViewById(R.id.recycler);
        lastUpdate=view.findViewById(R.id.lastUpdate);
        tvPrice=view.findViewById(R.id.previousPricee);
        tvPrevious=view.findViewById(R.id.percentage);


        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Gold");

        checkConnection();

        return view;

    }


    public void checkConnection(){
        if(isOnline()){
            new ParseData().execute();

//            handler.postDelayed(run,1000);
        }
    }

    public boolean isOnline(){
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                return false;
            }
        }

        return false;
    }



    Runnable run=new Runnable() {
        @Override
        public void run() {

            new ParseData().execute();


        }
    };



    private class ParseData extends AsyncTask<Void,Void,Void> {
        String tvRice;
        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if(!isOnline()){
                handler.removeCallbacks(run);
            }
            try {

                arrayUnit=new ArrayList<>();
                arrayPrice=new ArrayList<>();
                arrayPreviousPrice=new ArrayList<>();
                Document document = Jsoup.connect("http://www.goldpricedata.com/en/gold-prices-in-myanmar.php").get();


                Elements table=document.select("table[class=gradient-style]");

                Elements row=table.select("tr");
                Elements lastCol=row.get(0).select("p");
                 lastDate=lastCol.text();

                Elements priceColumn=row.get(2).select("th");
                priceTitle=priceColumn.get(1).text();
                previousPriceTitle=priceColumn.get(2).text();


                for(int i=3;i<row.size()-9;i++){
                    Elements cols=row.get(i).select("td");
                    Element eleUnitCol=cols.get(0);
                    Element elePriceCol=cols.get(1);
                    Element elePrevPriceCol=cols.get(2);

                    unit=eleUnitCol.text();
                     arrayUnit.add(unit);

                    String price=elePriceCol.text();
                     arrayPrice.add(price);
                    String previousPrice=elePrevPriceCol.text();
                      arrayPreviousPrice.add(previousPrice);
                }



                  Element percenRow=row.get(3);
                  Elements perceCols=percenRow.select("td");
                  Element elePercen=perceCols.get(3);
                  percentage=elePercen.text();







            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            lastUpdate.setText(lastDate);
            tvPrice.setText(priceTitle);
            tvPrevious.setText(previousPriceTitle);
            GoldAdapter goldAdapter=new GoldAdapter(arrayUnit,arrayPrice,arrayPreviousPrice);
            goldRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            goldRecycler.setHasFixedSize(true);
            goldRecycler.setAdapter(goldAdapter);




        }
    }
}

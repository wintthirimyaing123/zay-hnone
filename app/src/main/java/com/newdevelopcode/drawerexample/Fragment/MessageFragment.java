package com.newdevelopcode.drawerexample.Fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newdevelopcode.drawerexample.Adapter.CategoriesAdapter;
import com.newdevelopcode.drawerexample.R;
import com.newdevelopcode.drawerexample.model.Categories;

import java.util.ArrayList;

public class MessageFragment extends Fragment {
    View view;
    private RecyclerView mRecyclerView;
    private CategoriesAdapter cAdapter;
    ArrayList<Categories> list;
    String[] title= new String[]{
            "ဆန္ေဈးႏႈန္းမ်ား ","စားသုံးဆီေဈးႏႈန္းမ်ား ","ပဲေဈးႏႈန္းမ်ား ","ဟင္းခတ္အေမႊးအႀကိဳင္ေဈးႏႈန္းမ်ား","အသားငါးေဈးႏႈန္းမ်ား"
    };
    int image[]={R.drawable.rice,R.drawable.oil,R.drawable.pulses,R.drawable.species,R.drawable.fish};
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_message,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("စားေသာက္ကုန္ေဈးႏႈန္းမ်ား");

        mRecyclerView = view.findViewById(R.id.recycler);

        //Set the Layout Manager
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        mRecyclerView.setHasFixedSize(true);

        list=new ArrayList<>();
        for(int i=0;i<title.length;i++){
            Categories categories=new Categories(image[i],title[i]);
            list.add(categories);
        }
        //Initialize the ArrayLIst that will contain the data
        cAdapter = new CategoriesAdapter(list);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(cAdapter);
        cAdapter.setOnCvItemClickListener(new CategoriesAdapter.onCvItemClick() {
            @Override
            public void onItemClick(String name, int position) {
                switch (position){
                    case 0:
                        RiceFragment riceFragment=new RiceFragment();
                        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container,riceFragment);
                        fragmentTransaction.commit();
                        break;
                    case 1:
                        OilFragment oilFragment=new OilFragment();
                        FragmentTransaction fragmentTransaction2=getFragmentManager().beginTransaction();
                        fragmentTransaction2.replace(R.id.fragment_container,oilFragment);
                        fragmentTransaction2.commit();
                        break;

                    case 2:
                        PulsesFragment pulsesFragment=new PulsesFragment();
                        FragmentTransaction fragmentTransaction3=getFragmentManager().beginTransaction();
                        fragmentTransaction3.replace(R.id.fragment_container,pulsesFragment);
                        fragmentTransaction3.commit();
                        break;


                     case 3:
                        SpicesFragment spicesFragment=new SpicesFragment();
                        FragmentTransaction fragmentTransaction1=getFragmentManager().beginTransaction();
                        fragmentTransaction1.replace(R.id.fragment_container,spicesFragment);
                        fragmentTransaction1
                                .commit();
                        break;

                     case 4:
                        FishAndPrawnFragment fishAndPrawnFragment=new FishAndPrawnFragment();
                        FragmentTransaction fragmentTransaction4=getFragmentManager().beginTransaction();
                        fragmentTransaction4.replace(R.id.fragment_container,fishAndPrawnFragment);
                        fragmentTransaction4.commit();
                        break;
                 }
            }
        });

        //Get the data
         return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}

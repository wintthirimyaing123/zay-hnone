package com.newdevelopcode.drawerexample.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.R;
import com.newdevelopcode.drawerexample.model.Currencies;
import com.newdevelopcode.drawerexample.services.AllCurrencies;
import com.newdevelopcode.drawerexample.services.ApiUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoneyConverterFragment extends Fragment {
    public String[] rate=new String[38];
    String USD,AUD,BDT,BND,BRL,CAD,CHF,CNY,CZK,DKK,EGP,EUR,GBP,HKD,IDR,ILS,INR,JPY,KES,KHR,KRW,KWD,LAK,LKR,MYR,NOK,NPR,NZD,VND,PHP,PKR,RSD,RUB,SAR,SEK,SGD,THB,ZAR;
    EditText edMyanCurrency;
    String convertRate;
    TextView edCurrency;
    double newrate;
    int mynrate;
    Button btConvert;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view=inflater.inflate(R.layout.fragment_moneyconverter,null);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Money Converter");
        Spinner convertSpinner=view.findViewById(R.id.convertExchangerateConverter);
        edCurrency=view.findViewById(R.id.convert_edtext);
        edMyanCurrency=view.findViewById(R.id.convert_myan_edtext);
        btConvert=view.findViewById(R.id.bt_convert);

        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(getActivity()
                ,R.array.currencies_array,R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        convertSpinner.setAdapter(adapter);

        convertSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AllCurrencies allCurrencie = ApiUtil.getAllCurrencies();
                allCurrencie.getCurrencies().enqueue(new Callback<Currencies>() {
                    @Override
                    public void onResponse(Call<Currencies> call, Response<Currencies> response) {
                        if (response.isSuccessful()) {
                            USD = response.body().getRates().getUSD();
                            AUD = response.body().getRates().getAUD();
                            BDT = response.body().getRates().getBDT();
                            BND = response.body().getRates().getBND();
                            BRL = response.body().getRates().getBRL();
                            CAD = response.body().getRates().getCAD();
                            CHF = response.body().getRates().getCHF();
                            CNY = response.body().getRates().getCNY();
                            CZK = response.body().getRates().getCZK();
                            DKK = response.body().getRates().getDKK();

                            EGP = response.body().getRates().getEGP();
                            EUR = response.body().getRates().getEUR();
                            GBP = response.body().getRates().getGBP();
                            HKD = response.body().getRates().getHKD();
                            IDR = response.body().getRates().getIDR();
                            ILS = response.body().getRates().getILS();
                            INR = response.body().getRates().getINR();
                            JPY = response.body().getRates().getJPY();
                            KES = response.body().getRates().getKES();
                            KHR = response.body().getRates().getKHR();

                            KRW = response.body().getRates().getKRW();
                            KWD = response.body().getRates().getKWD();
                            LAK = response.body().getRates().getLAK();
                            LKR = response.body().getRates().getLKR();
                            MYR = response.body().getRates().getMYR();
                            NOK = response.body().getRates().getNOK();
                            NPR = response.body().getRates().getNPR();
                            NZD = response.body().getRates().getNZD();
                            PHP = response.body().getRates().getPHP();
                            PKR = response.body().getRates().getPKR();

                            RSD = response.body().getRates().getRSD();
                            RUB = response.body().getRates().getRUB();
                            SAR = response.body().getRates().getSAR();
                            SEK = response.body().getRates().getSEK();
                            SGD = response.body().getRates().getSGD();
                            THB = response.body().getRates().getTHB();
                            VND = response.body().getRates().getVND();
                            ZAR = response.body().getRates().getZAR();

                        }
                        rate= new String[]{AUD,BDT,BND,BRL,KHR,CAD,CNY,CZK,DKK,EGP,
                                EUR,HKD,INR,IDR,ILS,JPY,KES,KRW,
                                KWD,LAK,MYR,NPR,NZD,NOK,PKR,PHP,GBP,RUB,SAR
                                ,RSD,SGD,ZAR,LKR,SEK,CHF,THB,USD,VND};
                    }

                    public void onFailure(Call<Currencies> call, Throwable t) {
                    }
                });

                convertRate=rate[i];
                edCurrency.setText(convertRate);


            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });
        btConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currency=edCurrency.getText().toString();
                newrate= Double.parseDouble(currency);
                String mcurrency=edMyanCurrency.getText().toString();
                if(mcurrency.isEmpty()){
                    Toast.makeText(getActivity(), "Please Fill the Myanmar rate", Toast.LENGTH_SHORT).show();
                }
                else {
                    mynrate = Integer.parseInt(mcurrency);
                    Toast.makeText(getActivity(), "" + mynrate, Toast.LENGTH_SHORT).show();
                    edMyanCurrency.setText(String.valueOf(newrate * mynrate));
                }
            }
        });



        return view;
    }




}

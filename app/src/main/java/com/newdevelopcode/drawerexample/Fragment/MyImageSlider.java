package com.newdevelopcode.drawerexample.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.newdevelopcode.drawerexample.R;

public class MyImageSlider extends Fragment {
    int imageid;
    public static MyImageSlider newInstance(int id){
        MyImageSlider slider=new MyImageSlider();
        Bundle bundle=new Bundle();
        bundle.putInt("imageid",id);
        slider.setArguments(bundle);
        return slider;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageid=getArguments().getInt("imageid");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.adapterview,container,false);
        ImageView imageView=view.findViewById(R.id.myimage);
        imageView.setImageResource(imageid);
        return view;
    }
}

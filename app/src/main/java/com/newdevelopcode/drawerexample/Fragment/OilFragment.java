package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.OilCategoryAdapter;
import com.newdevelopcode.drawerexample.Adapter.RiceCategoryAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public  class OilFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView riceRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    ArrayList<String> dateArray;
    ArrayList<String> groundnutSpecialArray;
    ArrayList<String> groundnutMediumArray;
    ArrayList<String> sesamumArray;
    ArrayList<String> palmArray;

    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    int currentImageIndex=0;
    int[] imgResources={R.drawable.oil1,R.drawable.oil2,R.drawable.kyaw,R.drawable.group};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.oil_layout,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("စားသုံးဆီေဈးႏႈန္းမ်ား  ");

        riceRecyclerView=v.findViewById(R.id.riceRecycler);
        swipeRefreshLayout=v.findViewById(R.id.swipe);
        slidingimage=v.findViewById(R.id.imageview);
        final Handler myHandler=new Handler();
        final Runnable myUpdateResults=new Runnable() {
            @Override
            public void run() {
                AnimationSlideShow();
            }
        };
        int delay=1000;//delay for 1 second
        int period=1500;//repeat every 4 second
        Timer timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                myHandler.post(myUpdateResults);
            }
        },delay,period);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);

        checkConnection();
        return v;
    }
    private void AnimationSlideShow() {
        slidingimage.setImageResource(imgResources[currentImageIndex%imgResources.length]);
        currentImageIndex++;
    }

    public void checkConnection(){
        if(isOnline()){
            swipeRefreshLayout.setRefreshing(true);
            new ParseData().execute();

        }
    }

    public boolean isOnline(){
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return false;
    }

    Runnable run=new Runnable() {
        @Override
        public void run() {
            new ParseData().execute();
        }
    };
    private class ParseData extends AsyncTask<Void,Void,Void> {
        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if(!isOnline()){
                handler.removeCallbacks(run);
            }
            try {
                swipeRefreshLayout.setRefreshing(false);

                Document document= Jsoup.connect("https://www.csostat.gov.mm/market.asp").get();
                Elements table=document.select("table[class=btext1]");
                Elements row=table.select("tr");
                Element firstRow=row.get(7);
                Element secondRow=row.get(13);
                Element thirdRow=row.get(14);
                Element fourthRow=row.get(15);
                Element fifthRow=row.get(16);

                dateArray=new ArrayList<>();
                groundnutSpecialArray=new ArrayList<>();
                groundnutMediumArray=new ArrayList<>();
                sesamumArray=new ArrayList<>();
                palmArray=new ArrayList<>();

                Elements dateColumn=firstRow.select("td");
                for(int i=2;i<dateColumn.size();i++){
                    String date=dateColumn.get(i).text();
                    dateArray.add(date);
                }
                Elements groundnutSpecialColumn=secondRow.select("td");
                for(int i=2;i<groundnutSpecialColumn.size();i++){
                    String date=groundnutSpecialColumn.get(i).text();
                    groundnutSpecialArray.add(date);

                }
                Elements groundnutMediumColumn=thirdRow.select("td");
                for(int i=2;i<groundnutMediumColumn.size();i++){
                    String date=groundnutMediumColumn.get(i).text();
                    groundnutMediumArray.add(date);

                }
                Elements sesamumColumn=fourthRow.select("td");
                for(int i=2;i<sesamumColumn.size();i++){
                    String date=sesamumColumn.get(i).text();
                    sesamumArray.add(date);

                }
                Elements palmColumn=fifthRow.select("td");
                for(int i=2;i<palmColumn.size();i++){
                    String date=palmColumn.get(i).text();
                    palmArray.add(date);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            OilCategoryAdapter oilAdapter=new OilCategoryAdapter(dateArray,groundnutSpecialArray
                    ,groundnutMediumArray,sesamumArray,palmArray);
            riceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            riceRecyclerView.setHasFixedSize(true);
            riceRecyclerView.setAdapter(oilAdapter);

        }
    }
    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}

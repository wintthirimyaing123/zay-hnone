package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.PulsesCategoryAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PulsesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView pulsesRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    ArrayList<String> dateArray;
    ArrayList<String> gramArray;
    ArrayList<String> penilayArray;
    ArrayList<String> pegyiArry;
    ArrayList<String> satawpeArray;

    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    int currentImageIndex=0;
    int[] imgResources={R.drawable.pulsenew1,R.drawable.pulsenew3,
            R.drawable.pulsenew4,R.drawable.pulsenew5};


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.pulses_layout,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("ပဲေဈးႏႈန္းမ်ား");

        pulsesRecyclerView=v.findViewById(R.id.pulsesRecycler);
        swipeRefreshLayout=v.findViewById(R.id.swipe);
        slidingimage=v.findViewById(R.id.imageview);
        final Handler myHandler=new Handler();
        final Runnable myUpdateResults=new Runnable() {
            @Override
            public void run() {
                AnimationSlideShow();
            }
        };
        int delay=1000;//delay for 1 second
        int period=1500;//repeat every 4 second
        Timer timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                myHandler.post(myUpdateResults);
            }
        },delay,period);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);


        checkConnection();
        return v;
    }

    private void AnimationSlideShow() {
        slidingimage.setImageResource(imgResources[currentImageIndex%imgResources.length]);
        currentImageIndex++;
    }

    private void checkConnection() {
        if(isOnline()){
            new ParseDate().execute();
            swipeRefreshLayout.setRefreshing(false);

        }

    }

    public boolean isOnline() {
        if(getActivity()!=null && !getActivity().isFinishing()) {
            ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
        }
    Runnable run=new Runnable() {
        @Override
        public void run() {

           new ParseDate().execute();


        }
    };






    private class ParseDate extends AsyncTask<Void,Void,Void> {

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if (!isOnline()) {
                handler.removeCallbacks(run);
            }
            try {
                swipeRefreshLayout.setRefreshing(false);
                Document document = Jsoup.connect("https://www.csostat.gov.mm/market.asp").get();
                Elements table = document.select("table[class=btext1]");
                Elements row = table.select("tr");
                Element firstRow = row.get(7);
                Element secondRow = row.get(20);
                Element thirdRow = row.get(21);
                Element fourthRow = row.get(22);
                Element fifthRow = row.get(23);
                dateArray = new ArrayList<>();
                gramArray = new ArrayList<>();
                penilayArray = new ArrayList<>();
                pegyiArry = new ArrayList<>();
                satawpeArray = new ArrayList<String>();

                Elements dateColumn = firstRow.select("td");
                for (int i = 2; i < dateColumn.size(); i++) {
                    String date = dateColumn.get(i).text();
                    dateArray.add(date);

                }
                Elements gramColumn = secondRow.select("td");
                for (int i = 2; i < gramColumn.size(); i++) {
                    String date = gramColumn.get(i).text();
                    Log.d("test","the value is "+gramColumn.get(i));
                    gramArray.add(date);

                }
                Elements penilayColumn = thirdRow.select("td");
                for (int i = 2; i < penilayColumn.size(); i++) {
                    String date = penilayColumn.get(i).text();
                    penilayArray.add(date);

                }
                Elements pegyiColumn = fourthRow.select("td");
                for (int i = 2; i < pegyiColumn.size(); i++) {
                    String date = pegyiColumn.get(i).text();
                    pegyiArry.add(date);

                }
                Elements satawpeColumn = fifthRow.select("td");
                for (int i = 2; i < satawpeColumn.size(); i++) {
                    String date = satawpeColumn.get(i).text();
                    satawpeArray.add(date);

                }

                Log.d("value", "the value" + dateArray);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            PulsesCategoryAdapter pulsesAdapter=new PulsesCategoryAdapter(dateArray,gramArray,penilayArray,pegyiArry,satawpeArray);
            pulsesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            pulsesRecyclerView.setHasFixedSize(true);
            pulsesRecyclerView.setAdapter(pulsesAdapter);
        }


    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);

    }
}

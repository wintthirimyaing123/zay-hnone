package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import com.newdevelopcode.drawerexample.Adapter.RiceCategoryAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public  class RiceFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView riceRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    ArrayList<String> dateArray;
    ArrayList<String> emataArray;
    ArrayList<String> pawsanArray;
    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    int currentImageIndex=0;
    int[] imgResources={R.drawable.ricenew1,R.drawable.ricenew2,R.drawable.ricebnew3,
            R.drawable.ricenew4,R.drawable.ricenew5};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View v=inflater.inflate(R.layout.rice_layout,container,false);
         ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("ဆန္ေဈးႏႈန္းမ်ား");

        riceRecyclerView=v.findViewById(R.id.riceRecycler);
        swipeRefreshLayout=v.findViewById(R.id.swipe);
        slidingimage=v.findViewById(R.id.imageview);
        final Handler myHandler=new Handler ();
        final Runnable myUpdateResults=new Runnable() {
        @Override
        public void run() {
            AnimationSlideShow();
        }
    };
    int delay=1000;//delay for 1 Handlersecond
    int period=1500;//repeat every 4 second
    Timer timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
        @Override
        public void run() {
            myHandler.post(myUpdateResults);
        }
    },delay,period);



        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);


        checkConnection();
        return v;
    }

    private void AnimationSlideShow() {
        slidingimage.setImageResource(imgResources[currentImageIndex%imgResources.length]);
        currentImageIndex++;
    }



    public void checkConnection(){
        if(isOnline()){
            new ParseData().execute();

//            handler.postDelayed(run,1000);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public boolean isOnline(){
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
    }



    Runnable run=new Runnable() {
        @Override
        public void run() {

            new ParseData().execute();


        }
    };



    private class ParseData extends AsyncTask<Void,Void,Void> {
        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if(!isOnline()){
                handler.removeCallbacks(run);
            }
            try {
                swipeRefreshLayout.setRefreshing(false);

                Document document= Jsoup.connect("https://www.csostat.gov.mm/market.asp").get();
                Elements table=document.select("table[class=btext1]");
                Elements row=table.select("tr");
                Element firstRow=row.get(7);
                Element secondRow=row.get(8);
                Element thirdRow=row.get(9);
                dateArray=new ArrayList<>();
                emataArray=new ArrayList<>();
                pawsanArray=new ArrayList<>();

                Elements dateColumn=firstRow.select("td");
                for(int i=2;i<dateColumn.size();i++){
                    String date=dateColumn.get(i).text();
                    dateArray.add(date);

                }
                Elements emetaColumn=secondRow.select("td");
                for(int i=2;i<emetaColumn.size();i++){
                    String date=emetaColumn.get(i).text();
                    emataArray.add(date);

                }

                Elements pawsanColumn=thirdRow.select("td");
                for(int i=2;i<pawsanColumn.size();i++){
                    String date=pawsanColumn.get(i).text();
                    pawsanArray.add(date);

                }

                Log.d("value","the value"+dateArray);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            RiceCategoryAdapter riceAdapter=new RiceCategoryAdapter(dateArray,emataArray,pawsanArray);
            riceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            riceRecyclerView.setHasFixedSize(true);
            riceRecyclerView.setAdapter(riceAdapter);

        }
    }



    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}

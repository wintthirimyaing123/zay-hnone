package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.SpicesCategoryAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SpicesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView spicesrecyclerview;
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    ArrayList<String> dateArray;
    ArrayList<String> chilliesArray;
    ArrayList<String> garlicArray;
    ArrayList<String> onionArray;
    ArrayList<String> potatoArray;

    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    int currentImageIndex=0;
    int[] imgResources={R.drawable.spicy1,R.drawable.spicy5};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.spices_layout,container,false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("ဟင္းခတ္အေမႊးအႀကိဳင္ေဈးႏႈန္းမ်ား");

        spicesrecyclerview=v.findViewById(R.id.spicesrecycler);
        swipeRefreshLayout=v.findViewById(R.id.swipe);
        slidingimage=v.findViewById(R.id.imageview);
        final Handler myHandler=new Handler();
        final Runnable myUpdateResults=new Runnable() {
            @Override
            public void run() {
                AnimationSlideShow();
            }
        };
        int delay=1000;//delay for 1 second
        int period=1500;//repeat every 4 second
        Timer timer=new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                myHandler.post(myUpdateResults);
            }
        },delay,period);

        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setOnRefreshListener(this);


        checkConnection();
        return v;
    }
    private void AnimationSlideShow() {
        slidingimage.setImageResource(imgResources[currentImageIndex%imgResources.length]);
        currentImageIndex++;
    }
    private void checkConnection() {
        if(isOnline()){
            new ParseData().execute();

//            handler.postDelayed(run,1000);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private boolean isOnline() {
        if(getActivity()!=null && !getActivity().isFinishing()){
            ConnectivityManager manager= (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=manager.getActiveNetworkInfo();
            if(networkInfo!=null && networkInfo.isConnected()){
                return true;
            }
            else {
                Toast.makeText(getActivity(), "Your offline!!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;

    }

    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            new ParseData().execute();

        }
    };




    private class ParseData extends AsyncTask<Void,Void,Void> {
        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if(!isOnline()){
                handler.removeCallbacks(runnable);


            }
            try {
                swipeRefreshLayout.setRefreshing(false);

                Document document= Jsoup.connect("https://www.csostat.gov.mm/market.asp").get();
                Elements table=document.select("table[class=btext1]");
                Elements row=table.select("tr");
                Element firstRow=row.get(7);
                Element secondRow=row.get(27);
                Element thirdRow=row.get(28);
                Element fourthRow=row.get(29);
                Element fifthRow=row.get(30);
                dateArray=new ArrayList<>();
                chilliesArray=new ArrayList<>();
                garlicArray=new ArrayList<>();
                onionArray=new ArrayList<>();
                potatoArray=new ArrayList<>();

                Elements dateColumn=firstRow.select("td");
                for(int i=2;i<dateColumn.size();i++){
                    String date=dateColumn.get(i).text();
                    dateArray.add(date);

                }
                Elements gramColumn=secondRow.select("td");
                for(int i=2;i<gramColumn.size();i++){
                    String date=gramColumn.get(i).text();
                    chilliesArray.add(date);
                Log.d("spices ","Spices value is"+date);
                }

                Elements penilayColumn=thirdRow.select("td");
                for(int i=2;i<penilayColumn.size();i++){
                    String date=penilayColumn.get(i).text();
                    garlicArray.add(date);

                }

                Elements pegyiColumn=fourthRow.select("td");
                for(int i=2;i<pegyiColumn.size();i++){
                    String date=pegyiColumn.get(i).text();
                    onionArray.add(date);

                }

                Elements satawpeColumn=fifthRow.select("td");
                for(int i=2;i<satawpeColumn.size();i++){
                    String date=satawpeColumn.get(i).text();
                    potatoArray.add(date);

                }

                Log.d("value","the value"+dateArray);


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            SpicesCategoryAdapter spicesAdapter=new SpicesCategoryAdapter(dateArray,chilliesArray,garlicArray,onionArray,potatoArray);
            spicesrecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
            spicesrecyclerview.setHasFixedSize(true);
            spicesrecyclerview.setAdapter(spicesAdapter);

        }
    }



    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}

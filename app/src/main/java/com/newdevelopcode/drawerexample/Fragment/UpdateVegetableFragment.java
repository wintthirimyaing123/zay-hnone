package com.newdevelopcode.drawerexample.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Adapter.CurrentPriceAdapter;
import com.newdevelopcode.drawerexample.Adapter.CurrentRiceTypeAdapter;
import com.newdevelopcode.drawerexample.Adapter.GoldAdapter;
import com.newdevelopcode.drawerexample.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;

public class UpdateVegetableFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout swipeRefreshLayout;
    Handler handler;
    RecyclerView currentPriceRecycler;
    String[] type = {"ဆန္ေစ်းႏႈန္းမ်ား", "ပဲေစ်းႏႈန္းမ်ား", "ျကက္သြန္နီ္ေစ်းႏႈန္းမ်ား","ၾကက္သြန္ျဖဴေစ်းႏႈန္းမ်ား", "ျငဳပ္ေစ်းႏႈန္းမ်ား", "ထန္းလ်က္ေစ်းႏႈန္းမ်ား", "ၾကံသကာေစ်းႏႈန္းမ်ား", "မန္က်ည္းသီးေစ်းႏႈန္းမ်ား", "သၾကားေစ်းႏႈန္းမ်ား", "အာလူးေစ်းႏႈန္းမ်ား","ပုဇြန္ေျခာက္ေစ်းႏႈန္းမ်ား","ေရခ်ိဳငါးေျခာက္ေစ်းႏႈန္းမ်ား","ေရငံငါးေျခာက္ေစ်းႏႈန္းမ်ား"};
    String[] riceCategories={"ေျမာင္းျမ/ပုသိမ္ေပၚဆန္းသစ္","ဖ်ာပုံေပၚဆန္းသစ္","ေဒးဒရဲေပၚဆန္းသစ္","ေပၚဆန္းနယ္စုံသစ္","ၿမီးဒံု သစ္","ငစိန္ သစ္္","ေရႊဘုိေပၚဆန္း","ဧည့္မထ ၂၅ % သစ္","ေပၚဆန္းသစ္"};
    String[] pulsesCategories={"ကုလားပဲျဖဴလုံး","ကုလားပဲလုံးျဖဴႀကီး","စားေတာ္ပဲလုံး","စြန္တာနီ/ျပာ","ေထာပတ္ပဲ","ပဲႀကီး (ကုန္း)","ပဲတီစိမ္း ပခုကၠဴ","ပဲတီစိမ္း ခရမ္းေရႊ၀ါ (ထိပ္စ)","ပဲတီစိမ္း ပဲခူးေရႊ၀ါ","ပဲတီစိမ္း ၀ါးခယ္မ","ေျပာင္းဆံ","ပဲလြန္းျဖဴ မင္းလွ","ပဲလြန္းျဖဴ ျမစ္၀ကြ်န္းေပၚ","ပဲလြန္းနီ ျမစ္၀ကြ်န္းေပၚ","ဘုိကိတ္ပဲ","\n" +
            "မတ္ပဲ FAQ/RC (2018)","မတ္ပဲ SQ/RC (2018)"};
    String[] onionCategories={"ဆိပ္ျဖဴရွယ္ (သစ္)","ဆိပ္ျဖဴလတ္ၾကီး","ဆိပ္ျဖဴလတ္သန္႔","ဆိပ္ျဖဴလက္ေခ်ာ","ျမစ္သားရွယ္","ျမစ္သားလတ္ႀကီး","ျမစ္သားလတ္သန္","ျမစ္သားလတ္ေခ"};
   String[] garlicCategories={"ၾကဴကုတ္(ရွယ္)","ၾကဴကုတ္(လတ္)","ေတာင္ႀကီး ထူး-","ေတာင္ႀကီး ထူး-","ေတာင္ႀကီး ထူး-","ေတာင္ႀကီး ထူး-","ေအာင္ပန္း ထူး","ေအာင္ပန္း ထူး","ေအာင္ပန္း ထူး","ေအာင္ပန္း ထူး"};
    String[] chiliCategories={"ျငဳပ္ပြ(ဆင္ျဖူကၽြန္း)ပု","ျမစ္၀(ပု)","အိႏိၵယ(ရွည္)","အိမ္မဲ","ပန္းတေနာ္ရွည္","ေက်ာက္ဆည္ (ဒီမြန္)","ျမစ္၀ မိုးေထာင္"};
    String[] palm_sugarCategories={"ေတာင္သာ(ထန္းလ်က္)","ေညာင္ဦးေဆးရွယ္","မလုိင္ၾကမ္း","မလုိင္ေခ်ာ","မလုိင္၀ဲ","ေရစႀကဳိေခ်ာ","ပခုကၠဴအၾကမ္း"};
    String[] jaggeryCategories={"ၾကံသကာ (ျပည္)"};
    String[] tamarindCategories={"ေက်ာက္ပန္းေတာင္း(အသား)","ရမည္းသင္း(ပြင့္ကပ္)","မန္က်ည္းသီး (ဒါးဇင္း)"};
    String[] sugarCategories={"သၾကားအျဖဴ(ရွယ္)"};
    String[] potatoCategories={"ဟဲဟိုး ဆြဲ","ဟဲဟိုး A1","ဟဲဟိုး OK","ဟဲဟိုး S1","ဟဲဟိုး S2","ဆင္ျဖဴကၽြန္း ဆြဲ","ဆင္ျဖဴကၽြန္း လတ္","ဆင္ျဖဴကၽြန္း A1","ဆင္ျဖဴကၽြန္း OK","ဆင္ျဖဴကၽြန္း S1","ဆင္ျဖဴကၽြန္း S2",""};
    String[] pazunkyawtCategories={"ဖ်ာပံုေရႊပုဇြန္ရွယ္","ဖ်ာပံုေရႊပုဇြန္ရိုးရိုး","ဖ်ာပံုခ်ိဳႀကီးရွယ္","ဖ်ာပံုခ်ိဳႀကီးရိုးရိုး","ဖ်ာပံုခ်ိဳေသးရွယ္","ဖ်ာပံုခ်ိဳေသးရိုးရိုး","ၿမိတ္ပုဇြန္လတ္","ၿမိတ္ပုဇြန္ရိုးရိုး","ရခိုင္ေရႊပုဇြန္ရွယ္","ရခိုင္ေရႊပုဇြန္ရိုးရိုး","ရခိုင္ပုဇြန္လတ္",""};
    String[] freshwaterFishCategories={"ငါးက်ီး ၾကပ္တိုက္ႀကီး","ငါးက်ီး ၾကပ္တိုက္ေသး","ငါးေျမြထိုး","ငါးရံ႕ေျခာက္ႀကီး","ငါးရံ႕ေျခာက္လတ္","ငါးရံ႕လံုး"};
    String[] seawaterFishCategories={"ကလိန္","ငါးကြမ္းရွပ္","ငါးတံခြန္မွ်င္းႀကီး","ငါးတံခြန္လံုး","ငါးႏွပ္ခ်ိဳ","ငါးပုတ္သင္","ငါးေရာင္ႀကီး","ငါးေရာင္လတ္","ဆီငါးေရာင္","ငါးေရႊႀကီး","ငါးေရႊေသး","ငါးမီးတံသြယ္","ငါးဘူးစီ"};


    ArrayList<String> lowRicePriceArray,highRicePriceArray,pulsesPriceArray,onionPriceArray,garlicPriceArray,chiliPriceArray,palmSugarPriceArray,jaggeryPriceArray,tamarindPriceArray,sugarPriceArray,potatoPriceArray,pazunPriceArray,freshWaterFishPriceArray,seaWaterFishPriceArray;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.update_vegetable_layout, container, false);
        currentPriceRecycler = v.findViewById(R.id.current_price);
        swipeRefreshLayout=v.findViewById(R.id.swiperefresh);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("လက္ရွိကုန္ေစ်းႏွုန္းမ်ား");
        swipeRefreshLayout.setOnRefreshListener(this);
        checkConnection();


        return v;
    }

    public void checkConnection() {
        if (isOnline()) {
            new ParseData().execute();
        }
    }

    public boolean isOnline() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            } else {
                Toast.makeText(getActivity(), "Your offline!!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return false;
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {

            new ParseData().execute();

        }
    };

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private class ParseData extends AsyncTask<Void, Void, Void> {

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... voids) {
            if (!isOnline()) {
                handler.removeCallbacks(run);
            }
            try {

                Document document = Jsoup.connect("https://pissaya.info/commodity-prices/").get();
                Elements h4 = document.select("h4");
                Elements span = h4.select("span");
                Element eleDate = span.get(0);
                String date = eleDate.text();
                Elements table = document.select("table");
                Elements row = table.select("tr");
                //lowRicePriceArray = new ArrayList<>();
                highRicePriceArray = new ArrayList<>();
                pulsesPriceArray=new ArrayList<>();
                onionPriceArray=new ArrayList<>();
                garlicPriceArray=new ArrayList<>();
                chiliPriceArray=new ArrayList<>();
                palmSugarPriceArray=new ArrayList<>();
                jaggeryPriceArray=new ArrayList<>();
                tamarindPriceArray=new ArrayList<>();
                sugarPriceArray=new ArrayList<>();
                potatoPriceArray=new ArrayList<>();
                pazunPriceArray=new ArrayList<>();
                freshWaterFishPriceArray=new ArrayList<>();
                seaWaterFishPriceArray=new ArrayList<>();

                for (int i = 1; i < 10; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);
                    Element highColum = column.get(2);
                    //String lowRicePrice = eleColmn.text();
                    String highRicePrice = highColum.text();
                    //lowRicePriceArray.add(lowRicePrice);
                    highRicePriceArray.add(highRicePrice);

                }
                for (int i = 12; i < 29; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    pulsesPriceArray.add(price);
                    Log.d("test","value is"+price);


                }

                for (int i = 39; i < 47; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    onionPriceArray.add(price);
                    Log.d("test","omini value is "+price);


                }
                for (int i = 49; i < 59; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    garlicPriceArray.add(price);
                    Log.d("test","omini white value is "+price);


                }  for (int i = 61; i < 68; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    chiliPriceArray.add(price);
                    Log.d("test","chili value is "+price);


                }
                for (int i = 71; i < 78; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    palmSugarPriceArray.add(price);
                    Log.d("test","Palm Sugar value is "+price);


                }
                for (int i = 80; i < 81; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    jaggeryPriceArray.add(price);
                    Log.d("test","Jaggery  value is "+price);


                }

                for (int i = 83; i < 86; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    tamarindPriceArray.add(price);
                    Log.d("test","Temarind  value is "+price);


                }
                for (int i = 88; i < 89; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    sugarPriceArray.add(price);
                    Log.d("test","Sugar  value is "+price);


                }

                for (int i = 91; i < 102; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    potatoPriceArray.add(price);
                    Log.d("test","Patato  value is "+price);


                }

                for (int i = 104; i < 115; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    pazunPriceArray.add(price);
                    Log.d("test","Pazun  value is "+price);


                }
                for (int i = 117; i < 123; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    freshWaterFishPriceArray.add(price);
                    Log.d("test","fresh   value is "+price);

                }

                for (int i = 125; i < 138; i++) {
                    Elements column = row.get(i).select("td");
                    Element eleColmn = column.get(1);

                    String price = eleColmn.text();

                    seaWaterFishPriceArray.add(price);
                    Log.d("test","sea   value is "+price);


                }


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            CurrentPriceAdapter currentPriceAdapter = new CurrentPriceAdapter(type,riceCategories,pulsesCategories,onionCategories,
                    garlicCategories,chiliCategories,palm_sugarCategories,jaggeryCategories,tamarindCategories,sugarCategories,potatoCategories,pazunkyawtCategories,freshwaterFishCategories,seawaterFishCategories,highRicePriceArray,pulsesPriceArray,onionPriceArray,garlicPriceArray,chiliPriceArray,palmSugarPriceArray,jaggeryPriceArray,tamarindPriceArray,sugarPriceArray,potatoPriceArray,pazunPriceArray,freshWaterFishPriceArray,seaWaterFishPriceArray);
            currentPriceRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            currentPriceRecycler.setHasFixedSize(true);
            currentPriceRecycler.setAdapter(currentPriceAdapter);


        }


    }
}

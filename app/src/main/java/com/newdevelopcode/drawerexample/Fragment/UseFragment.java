package com.newdevelopcode.drawerexample.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.newdevelopcode.drawerexample.Adapter.ImageSwipeAdapter;
import com.newdevelopcode.drawerexample.R;

import java.util.ArrayList;
import java.util.List;

public class UseFragment extends Fragment {
    View view;
    ViewPager viewPager;
    List<Fragment> fragments;
    ImageSwipeAdapter imageswipeadapter;

    ArrayList<Integer> images = new ArrayList<Integer>() {{
        add(R.drawable.a);
        add(R.drawable.b);
        add(R.drawable.c);
        add(R.drawable.d);
        add(R.drawable.e);
    }};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_use, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("အသံုးျပဳပံု");

        viewPager = view.findViewById(R.id.viewpager);

        fragments = getFragments();

        imageswipeadapter = new ImageSwipeAdapter(getActivity().getSupportFragmentManager(),fragments);
        viewPager.setAdapter(imageswipeadapter);
        return view;

    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }*/

    private List<Fragment> getFragments() {
        List<Fragment> flist = new ArrayList<>();
        for (int i = 0; i < images.size(); i++) {
            flist.add(MyImageSlider.newInstance(images.get(i)));
        }
        return flist;
    }
}

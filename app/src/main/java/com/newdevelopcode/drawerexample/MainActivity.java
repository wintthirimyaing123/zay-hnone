package com.newdevelopcode.drawerexample;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.newdevelopcode.drawerexample.Fragment.AboutFragment;
import com.newdevelopcode.drawerexample.Fragment.DevelopedByFragment;
import com.newdevelopcode.drawerexample.Fragment.ExchangeRateFragment;
import com.newdevelopcode.drawerexample.Fragment.GoldFragment;
import com.newdevelopcode.drawerexample.Fragment.GraphChartFragment;
import com.newdevelopcode.drawerexample.Fragment.MessageFragment;
import com.newdevelopcode.drawerexample.Fragment.MoneyConverterFragment;
import com.newdevelopcode.drawerexample.Fragment.UpdateVegetableFragment;
import com.newdevelopcode.drawerexample.Fragment.UseFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer=findViewById(R.id.drawer_layout);
        NavigationView navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

       /* Menu menu=navigationView.getMenu();
        MenuItem vegetableItem=menu.findItem(R.id.nav_vegetable);
        vegetableItem.getIcon().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);*/


        toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if(savedInstanceState==null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                    , new MessageFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_vegetable);
        }
    }
    private Boolean exit=false;
    @Override
    public void onBackPressed() {
        if(exit){
            super.onBackPressed();
            return;
        }try {
            {
                FragmentManager fragmentManager=getSupportFragmentManager();
                Fragment fragment=fragmentManager.findFragmentByTag("HOME");
                if(fragment!=null){
                    if(fragment.isVisible()){
                        this.exit=true;
                        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    fragment=MessageFragment.class.newInstance();
                    getFragmentManager().popBackStack();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container,fragment,"HOME")
                            .commit();
                }
            }
        } catch (Exception e) {}
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit=false;
            }
        },2000);
        }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
           case R.id.nav_vegetable:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new MessageFragment()).commit();
                break;
            case R.id.nav_exchangerate:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new ExchangeRateFragment()).commit();                break;
            case R.id.nav_moneyconverter:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new MoneyConverterFragment()).commit();                break;
            case R.id.nav_gold:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new GoldFragment()).commit();                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new AboutFragment()).commit();                break;
            case R.id.nav_advice:

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setData(Uri.parse("email"));
                String[] s = {"eiphyuhtun005@gmail.com"};
                i.putExtra(Intent.EXTRA_EMAIL,s);
                i.putExtra(Intent.EXTRA_SUBJECT,"This is a Subject");
                i.putExtra(Intent.EXTRA_TEXT,"Hi send your mind");
                i.setType("message/rfc822");
                Intent chooser = Intent.createChooser(i,"Launch Email");
                startActivity(chooser);

                break;
            case R.id.nav_share:
                Intent sharingIntent=new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText="Check it out.Your message goes here";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT,"Subject here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,shareBodyText);
                startActivity(Intent.createChooser(sharingIntent,"Sharing Option"));

                break;
            case R.id.nav_currentVegetabl:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new UpdateVegetableFragment()).commit();
                break;
            case R.id.nav_use:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new UseFragment()).commit();                break;
            case R.id.nav_develop:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                        ,new DevelopedByFragment()).commit();                break;
            /*case R.id.nav_barchart:
                GraphChartFragment fragment=new GraphChartFragment();
                fragment(fragment);
                break;*/
            default: getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                    ,new MessageFragment()).commit(); break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void fragment(Fragment fragment){
        Fragment fragment1=fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container
                ,fragment1);

    }


}


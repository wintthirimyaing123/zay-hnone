
package com.newdevelopcode.drawerexample.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("USD")
    @Expose
    private String uSD;
    @SerializedName("LKR")
    @Expose
    private String lKR;
    @SerializedName("NZD")
    @Expose
    private String nZD;
    @SerializedName("CNY")
    @Expose
    private String cNY;
    @SerializedName("CHF")
    @Expose
    private String cHF;
    @SerializedName("VND")
    @Expose
    private String vND;
    @SerializedName("PHP")
    @Expose
    private String pHP;
    @SerializedName("KRW")
    @Expose
    private String kRW;
    @SerializedName("BDT")
    @Expose
    private String bDT;
    @SerializedName("EGP")
    @Expose
    private String eGP;
    @SerializedName("RSD")
    @Expose
    private String rSD;
    @SerializedName("MYR")
    @Expose
    private String mYR;
    @SerializedName("IDR")
    @Expose
    private String iDR;
    @SerializedName("KHR")
    @Expose
    private String kHR;
    @SerializedName("SGD")
    @Expose
    private String sGD;
    @SerializedName("NOK")
    @Expose
    private String nOK;
    @SerializedName("ILS")
    @Expose
    private String iLS;
    @SerializedName("SEK")
    @Expose
    private String sEK;
    @SerializedName("CZK")
    @Expose
    private String cZK;
    @SerializedName("JPY")
    @Expose
    private String jPY;
    @SerializedName("RUB")
    @Expose
    private String rUB;
    @SerializedName("KWD")
    @Expose
    private String kWD;
    @SerializedName("BRL")
    @Expose
    private String bRL;
    @SerializedName("HKD")
    @Expose
    private String hKD;
    @SerializedName("ZAR")
    @Expose
    private String zAR;
    @SerializedName("NPR")
    @Expose
    private String nPR;
    @SerializedName("CAD")
    @Expose
    private String cAD;
    @SerializedName("GBP")
    @Expose
    private String gBP;
    @SerializedName("PKR")
    @Expose
    private String pKR;
    @SerializedName("KES")
    @Expose
    private String kES;
    @SerializedName("THB")
    @Expose
    private String tHB;
    @SerializedName("DKK")
    @Expose
    private String dKK;
    @SerializedName("AUD")
    @Expose
    private String aUD;
    @SerializedName("SAR")
    @Expose
    private String sAR;
    @SerializedName("LAK")
    @Expose
    private String lAK;
    @SerializedName("INR")
    @Expose
    private String iNR;
    @SerializedName("BND")
    @Expose
    private String bND;
    @SerializedName("EUR")
    @Expose
    private String eUR;

    public String getUSD() {
        return uSD;
    }

    public void setUSD(String uSD) {
        this.uSD = uSD;
    }

    public String getLKR() {
        return lKR;
    }

    public void setLKR(String lKR) {
        this.lKR = lKR;
    }

    public String getNZD() {
        return nZD;
    }

    public void setNZD(String nZD) {
        this.nZD = nZD;
    }

    public String getCNY() {
        return cNY;
    }

    public void setCNY(String cNY) {
        this.cNY = cNY;
    }

    public String getCHF() {
        return cHF;
    }

    public void setCHF(String cHF) {
        this.cHF = cHF;
    }

    public String getVND() {
        return vND;
    }

    public void setVND(String vND) {
        this.vND = vND;
    }

    public String getPHP() {
        return pHP;
    }

    public void setPHP(String pHP) {
        this.pHP = pHP;
    }

    public String getKRW() {
        return kRW;
    }

    public void setKRW(String kRW) {
        this.kRW = kRW;
    }

    public String getBDT() {
        return bDT;
    }

    public void setBDT(String bDT) {
        this.bDT = bDT;
    }

    public String getEGP() {
        return eGP;
    }

    public void setEGP(String eGP) {
        this.eGP = eGP;
    }

    public String getRSD() {
        return rSD;
    }

    public void setRSD(String rSD) {
        this.rSD = rSD;
    }

    public String getMYR() {
        return mYR;
    }

    public void setMYR(String mYR) {
        this.mYR = mYR;
    }

    public String getIDR() {
        return iDR;
    }

    public void setIDR(String iDR) {
        this.iDR = iDR;
    }

    public String getKHR() {
        return kHR;
    }

    public void setKHR(String kHR) {
        this.kHR = kHR;
    }

    public String getSGD() {
        return sGD;
    }

    public void setSGD(String sGD) {
        this.sGD = sGD;
    }

    public String getNOK() {
        return nOK;
    }

    public void setNOK(String nOK) {
        this.nOK = nOK;
    }

    public String getILS() {
        return iLS;
    }

    public void setILS(String iLS) {
        this.iLS = iLS;
    }

    public String getSEK() {
        return sEK;
    }

    public void setSEK(String sEK) {
        this.sEK = sEK;
    }

    public String getCZK() {
        return cZK;
    }

    public void setCZK(String cZK) {
        this.cZK = cZK;
    }

    public String getJPY() {
        return jPY;
    }

    public void setJPY(String jPY) {
        this.jPY = jPY;
    }

    public String getRUB() {
        return rUB;
    }

    public void setRUB(String rUB) {
        this.rUB = rUB;
    }

    public String getKWD() {
        return kWD;
    }

    public void setKWD(String kWD) {
        this.kWD = kWD;
    }

    public String getBRL() {
        return bRL;
    }

    public void setBRL(String bRL) {
        this.bRL = bRL;
    }

    public String getHKD() {
        return hKD;
    }

    public void setHKD(String hKD) {
        this.hKD = hKD;
    }

    public String getZAR() {
        return zAR;
    }

    public void setZAR(String zAR) {
        this.zAR = zAR;
    }

    public String getNPR() {
        return nPR;
    }

    public void setNPR(String nPR) {
        this.nPR = nPR;
    }

    public String getCAD() {
        return cAD;
    }

    public void setCAD(String cAD) {
        this.cAD = cAD;
    }

    public String getGBP() {
        return gBP;
    }

    public void setGBP(String gBP) {
        this.gBP = gBP;
    }

    public String getPKR() {
        return pKR;
    }

    public void setPKR(String pKR) {
        this.pKR = pKR;
    }

    public String getKES() {
        return kES;
    }

    public void setKES(String kES) {
        this.kES = kES;
    }

    public String getTHB() {
        return tHB;
    }

    public void setTHB(String tHB) {
        this.tHB = tHB;
    }

    public String getDKK() {
        return dKK;
    }

    public void setDKK(String dKK) {
        this.dKK = dKK;
    }

    public String getAUD() {
        return aUD;
    }

    public void setAUD(String aUD) {
        this.aUD = aUD;
    }

    public String getSAR() {
        return sAR;
    }

    public void setSAR(String sAR) {
        this.sAR = sAR;
    }

    public String getLAK() {
        return lAK;
    }

    public void setLAK(String lAK) {
        this.lAK = lAK;
    }

    public String getINR() {
        return iNR;
    }

    public void setINR(String iNR) {
        this.iNR = iNR;
    }

    public String getBND() {
        return bND;
    }

    public void setBND(String bND) {
        this.bND = bND;
    }

    public String getEUR() {
        return eUR;
    }

    public void setEUR(String eUR) {
        this.eUR = eUR;
    }

}

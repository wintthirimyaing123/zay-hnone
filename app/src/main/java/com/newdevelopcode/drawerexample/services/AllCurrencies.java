package com.newdevelopcode.drawerexample.services;

import com.newdevelopcode.drawerexample.model.Currencies;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by USER on 11/28/2018.
 */

public interface AllCurrencies {
    @GET("api/latest")
    Call<Currencies> getCurrencies();
}

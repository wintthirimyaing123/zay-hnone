package com.newdevelopcode.drawerexample.services;

/**
 * Created by USER on 11/28/2018.
 */

public class ApiUtil {
    private static String link="https://forex.cbm.gov.mm";

    public static AllCurrencies getAllCurrencies(){
        return RetrofitClient.getClient(link).create(AllCurrencies.class);
    }
}

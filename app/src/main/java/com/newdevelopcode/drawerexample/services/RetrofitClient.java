package com.newdevelopcode.drawerexample.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 11/28/2018.
 */

public class RetrofitClient {
    private static Retrofit retrofit;

    public static Retrofit getClient(String BASE_URL){
        if(retrofit==null){
            retrofit=new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    return retrofit;
    }
}
